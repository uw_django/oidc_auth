# OIDC Auth for Django

This is a pretty niche little library, based *very* heavily on the work done by Ryan Goggin on [duo_auth](https://git.uwaterloo.ca/uw_django/duo_auth). It's a Django authentication backend that allows you to use an OpenID Connect provider as a source of authentication.

I'm putting this out as I need to switch **some** of my tools to *duo* OIDC auth, while some are remaining on ADFS's OIDC auth. This tool works with both (even both at the same time if we want to get crazy), allowing me to upgrade all of my software at once.

## Installation

No pypi package planned, so just install with `pip+https://git.uwaterloo.ca/uw_django/oidc_auth.git@VERSION_TAG`
...

## Configuration
Here are example configurations for ADFS OIDC, and DUO OIDC. You can also use any other OIDC provider, as long as it supports the `authorization_code` or `client_credentials` grant types.

```python
# Settings.py

INSTALLED_APPS = [
    # ...
    'oidc_auth',
    # If you want to use dyanmic group mappings/flags, you also need this:
    'oidc_auth.dynamic_map',
    # ...
]

# You must set the backend
AUTHENTICATION_BACKENDS = [
    'oidc_auth.backends.OIDCBackend',
    'django.contrib.auth.backends.ModelBackend',
    # If you want to use dyanmic group mappings/flags, use this instead:
    # 'oidc_auth.dynamic_map.backends.OIDCBackend',
]

# Note the url format
# In this case we're defaulting to the DUO login, but support ADFS as well
LOGIN_URL = 'oidc_auth:oidc_login-duo'
LOGOUT_URL = 'oidc_auth:oidc_logout-duo'

# Alternatively, if you just have 1 option or want to default to the FIRST:
LOGIN_URL = 'oidc_auth:oidc_login'
LOGOUT_URL = 'oidc_auth:oidc_logout'

# You can list as many clients as you want, but probably only want one
OIDC_CLIENTS = {
    'duo': {
        "auth_server": "https://sso-4ccc589b.sso.duosecurity.com/oidc/XXXXXCLIENTIDXXXXXXX",
        "username_claim": "winaccountname",
        "claim_mapping": {
            "first_name": "given_name",
            "last_name": "family_name",
            "email": "email",
        },
        "group_claim": "group",
        "group_mapping": {
            "special-users": ["uw-staff", "uw-faculty"],
        },
        "group_to_flag_mapping": {
            "is_staff": ["groupergroup-myapp-staff", "groupergroup-myapp-admin"],
            "is_superuser": "groupergroup-myapp-admin",
        },
        "auto_create_user": True,
        # If you want to set the users groups to ONLY the ones in the claim:
        # this will not create groups that don't exist in your database
        #"map_exact_groups": False, # (optional)
        "client_id": "XXXXXCLIENTIDXXXXXXX",
        "client_secret": "XXXXMYSECRETXXXXX",
        #"callback_url": "/oidc/duo/callback", #(optional)

        # If you want to do session timeout before redirecting to OIDC provider
        # (this does not allow more than 8 hour session)
        # By default, this is False and will use Django's default session expiry
        # for older versions of Django.  For 4.2 and above, this will use
        # the token expiry time.
        # "use_django_session_expiry": True
    },
    'adfs': {
        "auth_server": "https://adfstest.uwaterloo.ca/adfs",
        "username_claim": "winaccountname",
        "claim_mapping": {
            "first_name": "given_name",
            "last_name": "family_name",
            "email": "email",
        },
        "auto_create_user": True,
        "client_id": "XXXXXCLIENTIDXXXXXXX",
        # Don't provide client_secret for 'authorization_code' grant type
        # You can provide a custom callback URL if necessary.
        "callback_url": "/oauth2/callback/",
    }
}


# log oidc_auth user and ipaddress to console
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
        },
    },
    "loggers": {
        "oidc_auth.backends": {"level": "INFO"},
    },
    "root": {
        "level": "WARNING",
        "handlers": ["console"],
    },
}

```

### Session Expiry Time
In Django 4.2+, the session token expiry time will be set to the value **returned by the auth token** by default. In some cases this may be too short, or too long for security purposes.

To override the defaults, you can provide `use_django_session_expiry` to client configs. If set to `True` this will swap the timeout to django's existing `SESSION_COOKIE_AGE` setting.

By default this value defaults to *2 weeks*, which can be insecure for authenticated applications. As such, **oidc_auth will clamp the maximum session length to 8 hours, even if you set use_django_session_expiry**.

If you need sessions *longer* than 8 hours, you can increase this ceiling by setting `OIDC_MAX_SESSION_EXPIRY_TIME` to an integer in seconds.


### URLs

You'll need to register some URLS for everything to work:

```python
#urls.py

urlpatterns = [
    path('', include('oidc_auth.urls')),
    ...
]
```

This will register the following urls for each key (you'll likely need the callback url for configuring your OIDC provider)
- `/oidc/<key>/callback/`
- `/oidc/<key>/login/`
- `/oidc/<key>/logout/`

You can also provide a custom callback URL for a client if you need to:

```python
# settings.py

OIDC_CLIENTS = {
    'adfs': {
        # ...
        "callback_url": "/oauth2/callback/",
    }
}
```

Then, you can add a custom callback URL to your `urls.py`:

```python

# urls.py
from oidc_auth.views import oidc_callback

...
urlpatterns = [
    path('', include('oidc_auth.urls')),
    # your custom callback URL for adfs
    path ('oauth2/callback/', oidc_callback, {'client_name': 'adfs'}),
    #...
]

```

### URLs - Admin redirect

If you are using the Django Admin and want it to redirect to OIDC auth.

```python
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.http import urlencode
from django.conf import settings
from django.http import HttpResponse
def oidc_admin_login(request):
    if request.user.is_authenticated:
        # prevent redirect loop: /admin redirects to /admin/login when user without admin access
        return HttpResponse('Sorry. your user does not have admin access.')
    return redirect(f"{reverse_lazy(settings.LOGIN_URL)}?{urlencode({'next': request.GET.get('next', '/')})}")
def oidc_admin_logout(request):
    return redirect(reverse_lazy(settings.LOGOUT_URL))

urlpatterns = [
    path('admin/login/', oidc_admin_login),
    path('admin/logout/', oidc_admin_logout),
    ...
]
```


### Logging Out
OIDC doesn't officially have a standardized logout mechanism.

Some, however, provide an `end_session_endpoint` that you can use to log out. If your OIDC provider supports this, the library can automatically detect and utilize the endpoint. You can also manually set this if you have single-signout configured somewhere with `end_session_endpoint`

If the plugin can't find the end_session info, it will instead drop the current session for the user and force a prompt on the next login (to prevent auto-logging in with the same user immediately).


### Multiple OIDC Providers
As you've likely noticed from the example configuration, you can define multiple OIDC providers.

We don't provide a special login screen for multiple providers, but you can easily create your own which can allow users to select any of the `/oidc/<name>/login/` URLs to log in with.


### Groups
By specifying a `group_claim` you can also map groups to Django groups. This has some prerequisites to function:

- Groups need to be a ManyToMany field at `YourUserModel.groups`
- The group model needs to use `name` as a unique ID
- The group claim needs to be a list of strings, where each string is the name of a group



There's a couple different ways to use groups:

#### **Default:**
By default, if `group_claim` key is provided, the library will will check all the groups in the claim, and put the user in, or remove them from, the corresponding Django groups **which already exist in your database**. Groups will *not* be created automatically.


Note that if `group_claim` is set, and `map_exact_groups` is also set the library will *drop* users from any groups they've been manually assigned to in Django, as after `handle_groups` does a `set` comparison of existing groups and claim groups.


#### **Group Mapping:**


##### **In settings.py:**

You can set the `group_mapping` setting to map any number of claim groups to local groups. This is useful if you want to map a claim group to a Django group that doesn't exist in your database. For example, if you want to map the claim group `uw-staff` to the Django group `special-users`, you can do:

```python
# settings.py
OIDC_CLIENTS = {
    'adfs': {
        # ...
        "group_claim": "group",
        "group_mapping": {
            "special-users": "uw-staff",
            # NOTE, local group on LEFT
        }
    }
}

# Or, if you want to map multiple claim groups to a single Django group:
OIDC_CLIENTS = {
    'adfs': {
        # ...
        "group_claim": "group",
        "group_mapping": {
            "special-users": ["uw-staff", "uw-faculty"],
        }
    }
}

```

Note that this process **will** make any django groups that don't already exist.

##### **In the Django Admin:**

```python
# admin.py

INSTALLED_APPS = [
    # ...
    'oidc_auth',
    'oidc_auth.dynamic_map',
    # ...
]

AUTHENTICATION_BACKENDS = [
    'oidc_auth.dynamic_map.backends.OIDCBackend',
    'django.contrib.auth.backends.ModelBackend',
]
```


#### **Group to Flag Mapping:**


##### **In settings.py:**

Similarly with `group_to_flag_mapping`, you can also map claim groups to boolean flags on the user model. This is useful if you want to set a flag on the user model based on a claim group, but don't want to create a Django group for it. For example, if you want to set a `is_staff` flag on the user model based on the claim group `uw-staff`, you can do:

```python
# settings.py
OIDC_CLIENTS = {
    'adfs': {
        # ...
        "group_claim": "group",
        "group_to_flag_mapping": {
            "is_staff": "uw-staff",
            # NOTE, flag on LEFT
        }
    }
}
```
Like `group_mapping`, you can provide a list of claim groups to map to a single flag.


##### **In the Django Admin:**

```python
# admin.py

INSTALLED_APPS = [
    # ...
    'oidc_auth',
    'oidc_auth.dynamic_map',
    # ...
]

AUTHENTICATION_BACKENDS = [
    'oidc_auth.dynamic_map.backends.OIDCBackend',
    'django.contrib.auth.backends.ModelBackend',
]
```

## Technical Notes

The library will automatically create users if you set `auto_create_user` to `True` in the client config. It will also update the user's name and email if they change in the OIDC provider.

The library uses `Authlib` for the heavy lifting. Any settings you provide in the `OIDC_CLIENTS` dict will be passed directly to authlib's django oidc client (`AUTHLIB_OAUTH_CLIENTS`). See the [Authlib docs](https://docs.authlib.org/en/latest/client/django.html) for more info.


## Additional Notes:

Note that the community version of Authlib is **BSD licensed**. This library is **MIT licensed**.

AuthLib also has commercial licensing options available, if you need them. See [authlib.org](https://authlib.org/plans) and their [sustainability](https://docs.authlib.org/en/latest/community/sustainable.html) page for more info.
