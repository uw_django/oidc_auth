from setuptools import setup

'''
This project uses pyproject.toml instead of setup.py
This file is here to satisfy requirements of legacy environments
'''
setup()