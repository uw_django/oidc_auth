from django.conf import settings
from warnings import warn

__all__ = []

# Update settings so the token refresh functions work auto-magically
# via authlib's django integration signals
# mirror ALL settings into AUTHLIB_OAUTH_CLIENTS as well
authlib_clients = getattr(settings, 'AUTHLIB_OAUTH_CLIENTS', {})
for name, client in settings.OIDC_CLIENTS.items():
    if client.get('override_session_expiry', False):
        warn("override_session_expiry setting renamed to use_django_session_expiry", DeprecationWarning, stacklevel=2)
    if name not in authlib_clients:
        authlib_clients[name] = client
    
setattr(settings, 'AUTHLIB_OAUTH_CLIENTS', authlib_clients)

