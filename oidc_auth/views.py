from django.shortcuts import render, resolve_url, redirect
from django.contrib.auth import REDIRECT_FIELD_NAME, authenticate, login, logout
from django.urls import reverse
from django.conf import settings
from django.utils.http import urlencode
from oidc_auth.oauth import get_oauth_client
try:
    from django.utils.http import is_safe_url
except ImportError:
    from django.utils.http import url_has_allowed_host_and_scheme as is_safe_url

def get_redirect_url(request):
    redirect_to = request.POST.get(
        REDIRECT_FIELD_NAME,
        request.GET.get(REDIRECT_FIELD_NAME, '')
    )
    url_is_safe = is_safe_url(
        url=redirect_to,
        allowed_hosts={request.get_host()},
        require_https=request.is_secure(),
    )
    
    if not url_is_safe:
        redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

    return redirect_to


def oidc_login(request, client_name):
    oauth = get_oauth_client()
    client = getattr(oauth, client_name)
    # get the next url from the request
    next_page = request.GET.get('next', None)
    # save the next url in the session
    request.session['next'] = next_page

    callback_url = request.build_absolute_uri(
        settings.OIDC_CLIENTS[client_name].get(
            'callback_url',
            reverse('oidc_auth:oidc_callback', args=[client_name])
        )
    )
    if request.session.get('_force_oidc_reauth', False):
        return client.authorize_redirect(request, callback_url, prompt='login')
    return client.authorize_redirect(request, callback_url)


def oidc_callback(request, client_name):
    print(request.POST, request.GET)
    user = authenticate(request, oidc_client_name=client_name)
    if user and user.is_active:
        login(request, user)
    next_page = request.session.pop('next', None)
    if next_page is None:
        next_page = settings.LOGIN_REDIRECT_URL
    return redirect(next_page)


def oidc_logout(request, client_name):
    oauth = get_oauth_client()
    client = getattr(oauth, client_name)
    logout_url = settings.OIDC_CLIENTS[client_name].get(
        'end_session_endpoint',
        client.load_server_metadata().get('end_session_endpoint', None)
    )
    redirect_uri = str(
        request.build_absolute_uri(
            reverse('oidc_auth:oidc_logged_out')
        )
    ).lower()

    # Clear our session and log out even if this client doesn't have a logout URL.
    logout(request)
    request.session.flush()


    # If we know where to log out...
    if logout_url:
        # id_token_hint is too big, we'll not include it
        # redirect_uri = f"{logout_url}?{urlencode({'id_token_hint': oidc_auth_token, 'post_logout_redirect_uri': redirect_uri})}"
        redirect_uri = f"{logout_url}?{urlencode({'post_logout_redirect_uri': redirect_uri})}"
    else:
        request.session['_force_oidc_reauth'] = True
    return redirect(redirect_uri)


def oidc_logged_out(request):
    return render(request, 'oidc_auth/logged_out.html')
