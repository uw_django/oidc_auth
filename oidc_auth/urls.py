from django.urls import path
import oidc_auth.views as views
from django.conf import settings
app_name = 'oidc_auth'


# This is a list of lists of paths, one list for each client
# We do this because LOGIN_URL and LOGOUT_URL can't take parameters, but need to be reverse-able
dynamic_paths = [ [
    path(f'oidc/{key}/login/', views.oidc_login, {'client_name': key}, name=f'oidc_login-{key}'),
    path(f'oidc/{key}/callback/', views.oidc_callback, {'client_name': key}, name=f'oidc_callback-{key}'),
    path(f'oidc/{key}/logout/', views.oidc_logout, {'client_name': key}, name=f'oidc_logout-{key}'),
    ] for key in settings.OIDC_CLIENTS.keys() ] 

dynamic_paths = sum(dynamic_paths, [])  # Flatten

first_option = list(settings.OIDC_CLIENTS.keys())[0]

# We build normal paths because sane people would rather redirect without string concat
urlpatterns = [
    path(f'oidc/<str:client_name>/login/', views.oidc_login, name=f'oidc_login'),
    path(f'oidc/<str:client_name>/callback/', views.oidc_callback, name=f'oidc_callback'),
    path(f'oidc/<str:client_name>/logout/', views.oidc_logout, name=f'oidc_logout'),

    # Finally, set default ones
    path(f'oidc/login/', views.oidc_login, {'client_name': first_option}, name=f'oidc_login'),
    path(f'oidc/callback/', views.oidc_callback, {'client_name': first_option}, name=f'oidc_callback'),
    path(f'oidc/logout/', views.oidc_logout, {'client_name': first_option}, name=f'oidc_logout'),

    path('oidc/logged-out/', views.oidc_logged_out, name='oidc_logged_out'),
] + dynamic_paths