from oidc_auth.oauth import get_oauth_client
from authlib.integrations.base_client.errors import MismatchingStateError
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import timezone
from datetime import timedelta
from django import get_version
import logging

logger = logging.getLogger(__name__)


user_model = get_user_model()
major_django_version = int(get_version().split('.')[0])
minor_django_version = int(get_version().split('.')[1])
enable_session_expiry = major_django_version >= 4 and minor_django_version >= 2


# Helper to match function signature of update_or_create
def update_if_exists(defaults={}, **kwargs):
    """
    Update a user if it exists, otherwise return None

    :param defaults: dict of fields to update
    :param kwargs: fields to filter by, handles the username field being different
    :return: user, created
    """
    try:
        user = user_model.objects.get(kwargs)
        user.__dict__.update(defaults)
        user.save()
        return user, False
    except user_model.DoesNotExist:
        return None, False


class OIDCBackend(ModelBackend):
    def get_client_settings(self, oidc_client_name=None):
        client_settings = settings.OIDC_CLIENTS.get(oidc_client_name)
        if client_settings is None:
            raise Exception(f'No OIDC_CLIENTS settings for {oidc_client_name}')
        return client_settings

    def handle_user(self, username, user_fields, oidc_client_name=None):
        client_settings = self.get_client_settings(oidc_client_name=oidc_client_name)
        update_fn = user_model.objects.update_or_create if \
            client_settings.get('auto_create_user') else update_if_exists

        user_kwargs = {
            getattr(user_model, 'USERNAME_FIELD', 'username'): username
        }

        return update_fn(defaults=user_fields, **user_kwargs)

    def handle_groups(self, user, groups, oidc_client_name=None):
        client_settings = self.get_client_settings(oidc_client_name=oidc_client_name)
        group_flags = client_settings.get('group_to_flag_mapping', None)
        if group_flags:
            self.handle_group_flag_mapping(user, group_flags, groups, oidc_client_name=oidc_client_name)
        
        # For the rest of this we'll actually need a group model
        try:
            group_model = user_model.groups.rel.model
        except AttributeError:
            raise Exception('User model does not have groups relation')

        # We won't create groups, just get the existing ones and map 'em
        map_exact_groups = client_settings.get('map_exact_groups', False)
        if map_exact_groups:
            group_qs = group_model.objects.filter(name__in=groups)
            user.groups.set(group_qs)

        group_mapping = client_settings.get('group_mapping')
        if group_mapping:
            self.handle_group_mapping(user, group_mapping, groups, oidc_client_name=oidc_client_name)

    def handle_group_mapping(self, user, group_mapping, groups, oidc_client_name=None):
        group_set = set(groups)
        group_model = user_model.groups.rel.model

        # Create groups if they don't exist
        to_add = []
        for key, val in group_mapping.items():
            if isinstance(val, list):
                val_set = set(val)
            else:
                val_set = set([val])
            if group_set.intersection(val_set):
                group, _ = group_model.objects.get_or_create(name=key)
                to_add.append(group)
        if to_add:
            user.groups.add(*to_add)

    def handle_group_flag_mapping(self, user, group_flags, groups, oidc_client_name=None):
        group_set = set(groups)
        for key, val in group_flags.items():
            if isinstance(val, list):
                val_set = set(val)
            else:
                val_set = set([val])
            if group_set.intersection(val_set):
                setattr(user, key, True)
            else:
                setattr(user, key, False)
        user.save()

    def authenticate(self, request, oidc_client_name=None):
        if request.META.get('HTTP_X_FORWARDED_FOR'):
            ip = request.META.get('HTTP_X_FORWARDED_FOR').split(',')[0]
        else:
            ip = record.request.META.get('REMOTE_ADDR')
        client_settings = self.get_client_settings(oidc_client_name=oidc_client_name)
        oauth = get_oauth_client()
        client = getattr(oauth, oidc_client_name)
        proxies = getattr(client_settings, 'proxies', None)
        # Clean up first!
        try:
            token = client.authorize_access_token(request, proxies=proxies)
        except MismatchingStateError as ex:
            logger.error(f'authenticate failed CSRF check for ip:{ip}')
            return None
        # Allow overriding the session expiry for Django >= 4.2
        use_django_expiry = (
            client_settings.get('use_django_session_expiry', False) 
            or
            client_settings.get('override_session_expiry', False)
        )
        username_claim = client_settings.get('username_claim', 'user_id')
        claim_mapping = client_settings.get('claim_mapping', {})
        username = token['userinfo'][username_claim]
        user_fields = {}
        for key, value in claim_mapping.items():
            if value in token['userinfo']:
                user_fields[key] = token['userinfo'][value]
        user, _ = self.handle_user(username, user_fields, oidc_client_name=oidc_client_name)
        if user:
            group_claim = client_settings.get('group_claim', None)
            if group_claim is not None and group_claim in token['userinfo']:
                self.handle_groups(user, token['userinfo'][group_claim], oidc_client_name=oidc_client_name)
            request.session.pop('_force_oidc_reauth', None)
            # Not all session serializers like timezone.
            if enable_session_expiry:
                if use_django_expiry:
                    expiry = request.session.get_session_cookie_age()
                    max_expiry = getattr(settings, 'OIDC_MAX_SESSION_EXPIRY_TIME', 28800)
                    if expiry > max_expiry:
                        expiry = max_expiry
                    request.session.set_expiry(expiry)
                else:
                    request.session.set_expiry(timezone.now() + timedelta(seconds=token['expires_in']))
        logger.info(f'authenticated user:{username} ip:{ip}')
        return user
