from authlib.integrations.django_client import OAuth
from django.conf import settings


class Client:
    def __init__(self):
        self._oauth = OAuth()
        for name,client in settings.OIDC_CLIENTS.items():
            self._oauth.register(
                name=name,
                server_metadata_url=client['auth_server'] + client.get('openid_configuration_path', '/.well-known/openid-configuration'),
                client_kwargs={
                    'scope': client.get('claims', 'openid email profile'),
                    'proxies': getattr(settings, 'auth_proxies', None)
                },
            )

    @property
    def oauth(self):
        return self._oauth


default_client = Client()


def get_oauth_client():
    return default_client.oauth