from oidc_auth.backends import OIDCBackend as BaseOIDCBackend
from django.contrib.auth import get_user_model
import logging

logger = logging.getLogger(__name__)

from .models import SyncGroup, SyncGroupFlag

user_model = get_user_model()
try:
    group_model = user_model.groups.rel.model
except AttributeError:
    group_model = None


class OIDCBackend(BaseOIDCBackend):
    def handle_dynamic_flag_mapping(self, user, groups, oidc_client_name=None):
        """
        This function will handle the dynamic flag mapping. It will set the flags on the user
        based on the flags set in the database.  Unset flags take precedence over set flags.
        """
        sync_flags = SyncGroupFlag.objects.filter(
            oidc_client_name=oidc_client_name, identifier__in=groups
        )

        flags = {
            "is_active": None,
            "is_staff": None,
            "is_superuser": None,
        }

        for flag in sync_flags:
            for flag_name in flags:
                if getattr(flag, f"flag_{flag_name}"):
                    if flags[flag_name] != SyncGroupFlag.UNSET_FLAG:
                        flags[flag_name] = flag.set_flag

        for flag, value in flags.items():
            if value == SyncGroupFlag.SET_FLAG:
                setattr(user, flag, True)

            if value == SyncGroupFlag.UNSET_FLAG:
                setattr(user, flag, False)

        user.save()

    def handle_dynamic_group_mapping(self, user, groups, oidc_client_name=None):
        """
        This function will handle the dynamic group mapping. It will set the groups on the user
        based on the groups set in the database.  If the group is specified in a sync group,
        and the user is not in the group, they will be added to the group.
        """

        sync_groups = SyncGroup.objects.filter(
            oidc_client_name=oidc_client_name,
            identifier__in=groups,
        ).all()

        user_groups_with_sync_settings = []

        for g in user.groups.all():
            if g.oidc_sync_groups.count() > 0:
                user_groups_with_sync_settings.append(g)

        distinct_sync = set([x.group for x in sync_groups])

        remove_groups = set(user_groups_with_sync_settings).difference(distinct_sync)
        add_groups = distinct_sync.difference(set(user_groups_with_sync_settings))

        for g in remove_groups:
            user.groups.remove(g)

        for g in add_groups:
            user.groups.add(g)

    def handle_groups(self, user, groups, oidc_client_name=None):
        logger.info(f"Handling groups for {user} with groups {groups}")

        client_settings = self.get_client_settings(oidc_client_name=oidc_client_name)

        self.handle_dynamic_flag_mapping(
            user, groups, oidc_client_name=oidc_client_name
        )

        # For the rest of this we'll actually need a group model
        if group_model is None:
            raise Exception("User model does not have groups relation")

        # We won't create groups, just get the existing ones and map 'em
        map_exact_groups = client_settings.get("map_exact_groups", False)
        if map_exact_groups:
            group_qs = group_model.objects.filter(name__in=groups)
            user.groups.set(group_qs)

        self.handle_dynamic_group_mapping(
            user, groups, oidc_client_name=oidc_client_name
        )
