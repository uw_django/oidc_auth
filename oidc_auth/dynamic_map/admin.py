from django.contrib import admin
from .models import SyncGroup, SyncGroupFlag


@admin.register(SyncGroup)
class SyncGroupAdmin(admin.ModelAdmin):
    list_display = ("identifier", "group", "oidc_client_name")
    list_filter = ("oidc_client_name",)
    search_fields = ("identifier", "group__name", "oidc_client_name")

    class Meta:
        verbose_name = "Sync Group Configuration"
        verbose_name_plural = "Sync Group Configurations"


@admin.register(SyncGroupFlag)
class SyncGroupFlagAdmin(admin.ModelAdmin):
    list_display = ("description", "identifier", "oidc_client_name", "flag_is_active", "flag_is_staff", "flag_is_superuser")
    list_filter = ("oidc_client_name", "flag_is_active", "flag_is_staff", "flag_is_superuser")
    search_fields = ("description", "identifier", "oidc_client_name")

    class Meta:
        verbose_name = "Sync Group Flag Configuration"
        verbose_name_plural = "Sync Group Flag Configurations"