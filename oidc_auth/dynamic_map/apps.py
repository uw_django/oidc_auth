from django.apps import AppConfig


class DynamicMapConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "oidc_auth.dynamic_map"
    verbose_name = "OIDC Dynamic Group Map"
