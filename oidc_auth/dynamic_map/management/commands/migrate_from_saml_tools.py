from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

import logging

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Migrates sync groups from uw_saml_tools to oidc_auth.dynamic_map"

    def add_arguments(self, parser):
        parser.add_argument(
            "-c",
            "--client",
            dest="client",
            required=True,
            help="OIDC client name (eg: duo)",
        )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        client = options["client"]

        if verbosity >= 2:
            root_logger = logging.getLogger("")
            root_logger.setLevel(logging.DEBUG)

        installed_apps = settings.INSTALLED_APPS
        logger.warn(f"Installed apps: {installed_apps}")

        if "oidc_auth.dynamic_map" not in installed_apps:
            logger.warn("oidc_auth.dynamic_map is not in INSTALLED_APPS")
            raise CommandError("oidc_auth.dynamic_map is not in INSTALLED_APPS")

        if "uw_saml_tools" not in installed_apps:
            logger.warn("uw_saml_tools is not in INSTALLED_APPS")
            raise CommandError("uw_saml_tools is not in INSTALLED_APPS")

        from uw_saml_tools.models import SyncGroup as OldSyncGroup
        from oidc_auth.dynamic_map.models import SyncGroup

        logger.warn(f"OldSyncGroup: {OldSyncGroup.objects.all()}")

        for old_sync_group in list(OldSyncGroup.objects.all()):
            logger.warn(f"Creating SyncGroup for {old_sync_group.identifier}")

            SyncGroup.objects.create(
                identifier=old_sync_group.identifier,
                oidc_client_name=client,
                group=old_sync_group.group,
            )
