from django.db import models
from django.contrib.auth import get_user_model

user_model = get_user_model()
group_model = user_model.groups.rel.model


class SyncGroup(models.Model):
    identifier = models.CharField(
        max_length=1024,
        null=False,
        blank=False,
        help_text="The identifier for the group (Eg: FAST-dev)",
    )
    group = models.ForeignKey(
        group_model,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="oidc_sync_groups",
        help_text="The group to sync",
    )
    oidc_client_name = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        help_text="The name of the OIDC client to sync this group for",
    )

    def __str__(self):
        return f"{self.identifier} -> {self.group.name} ({self.oidc_client_name})"


class SyncGroupFlag(models.Model):
    UNSET_FLAG = "U"
    SET_FLAG = "S"

    FLAG_CHOICES = [
        (UNSET_FLAG, "Unset Flag"),
        (SET_FLAG, "Set Flag"),
    ]

    description = models.CharField(
        max_length=255, null=False, blank=False, help_text="Description of the action"
    )
    identifier = models.CharField(
        max_length=1024,
        null=False,
        blank=False,
        help_text="The identifier for the group (Eg: FAST-dev)",
    )
    oidc_client_name = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        help_text="The name of the OIDC client to sync this group for",
    )

    flag_is_active = models.BooleanField(
        default=False, help_text="Set the is_active flag if they are in the group"
    )
    flag_is_staff = models.BooleanField(
        default=False, help_text="Set the is_staff flag if they are in the group"
    )
    flag_is_superuser = models.BooleanField(
        default=False, help_text="Set the is_superuser flag if they are in the group"
    )

    set_flag = models.CharField(
        max_length=1,
        choices=FLAG_CHOICES,
        default=SET_FLAG,
        help_text="Whether to set or unset the flag",
    )

    def __str__(self):
        return f"{self.identifier} ({self.oidc_client_name})"
